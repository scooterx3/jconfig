#!/bin/bash
echo "Joomla reconfigurator (beta)"

function _clean_param() {   
	sed "s/.*'\(.*\)'.*/\1/" 
}

jcfg() {

	OPTIND=1
	backupfile='configuration_original_.php'
	
	if [[ $1 == '-h' ]]; then
		echo "Now vomiting helpful information..."

		cat <<HERE
		
jcfg (Joomla configurator)

Reconfigures the configuration.php file so you don't have to! 

Must be run from within the same directory as a configuration.php file. 

3 modes / flags: 
  -a auto (default)
  -m manual
  -c current

-a auto (default)

	'jcfg -a' or just 'jcfg', since it defaults to auto. Then the script
	spits out what the old credentials were and what the new credentials are.
	It will also spit out the siteURL and auto-replace log and tmp paths. 


-m manual

	'jcfg -m'. Then the user is prompted for at least a new database name
	and user name. If no password is given, a new one is generated. If no
	host is given, it defaults to 'localhost'.
	It will also spit out the siteURL and auto-replace log and tmp paths.


-c current

	'jcfg -c'. Then it spits out a single line of what the current credentials are in the
	configuration.php file.

HERE
		return
	fi

	unset dbhost_new dbname_new dbpass_new dbuser_new mode debug dbname_old_temp dbuser_old_temp livesite_new temp_path_new log_path_new 

	while getopts ":amcdh" opt; do
		case $opt in
			a)
				mode='auto' ;;
			m)
				mode='manual' ;;
			u)
				mode='url' ;;
			c) 
				mode='current' ;;
			d)
				debug='true' ;;
		esac
	done
	shift $((OPTIND-1))
	OPTIND=1

	if [[ ! -f configuration.php ]]; then
		
		echo "Can't find configuration.php!"
		return
	fi


	cp -n configuration.php $backupfile

	# What happens when I get a blank? The live_site thing is empty in many cases. Are the paths ever potentially blank?
	interesting_lines=$(egrep '.*\s*[^/]*(\$mosConfig_|var|public)\s*\$*(host|user[^a]|password|db[^tp]|log_path|tmp_path|live_site)' configuration.php)
	db_lines=$(egrep '.*\s*[^/]*(\$mosConfig_|var|public)\s*\$*(host|user|password|db)' <<< "$interesting_lines" | sort -k 2,2 | _clean_param)
	#read -r dbname_old dbhost_old dbpass_old dbuser_old <<< $(egrep '\s*(\$mosConfig_|var|public)\s*\$*(host|user[^a]|password|db[^tp])' configuration.php | sort -k 2,2 | _clean_param )

	dbname_old=$(head -1 <<< "$db_lines" )
	dbhost_old=$(head -2 <<< "$db_lines" | tail -1)
	dbpass_old=$(head -3 <<< "$db_lines" | tail -1)
	dbuser_old=$(tail -1 <<< "$db_lines")


	read -r dbname_old dbhost_old dbpass_old dbuser_old <<< $( _clean_param <<< "$db_lines" )

	live_site_old=$(grep live_site <<< "$interesting_lines" | _clean_param)
	log_path_old=$(grep log_path <<< "$interesting_lines" | _clean_param)
	tmp_path_old=$(grep tmp_path <<< "$interesting_lines" | _clean_param)

	# Catch circumstances in which one of the critical _old variables is empty, and abort.
	# I just don't care about live_site
	# I'm not sure if I care about log_path and tmp_path yet though. 
	if [[ -z $dbuser_old ]]; then
		echo -e "Empty variable detected, aborting! Hopefully this helps:\n"
		egrep "\s*(\$mosConfig_|var|public)\s*\$*(host|user[^a]|password|db[^tp]|log_path|tmp_path)" configuration.php | egrep "['\"]{2}"
		return
	fi


	# setting them to temp variables in case I run into default values ("username_here") down the road
	# I may not need this for Joomla
	dbname_old_temp=$dbname_old
	dbuser_old_temp=$dbuser_old

	if [[ $mode == 'current' ]]; then
#CURRENT
	# the point is to spit out a line I can use in creating the DB... not sure if needed. Might change this to just display the variables and done. 
		if [[ $dbhost_old == 'localhost' ]]; then dbhost_old=''; fi

		echo -e "$dbname_old $dbuser_old $dbpass_old $dbhost_old\n\nlivesite: $live_site_old \nlog_path: $log_path_old\ntmp_path: $tmp_path_old"
		return
	fi

	# what in the world are these for?! Testing, perhaps? Junk? 
	random_name_string=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 5 | head -n 1)
	random_pw_string=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 6 | head -n 1)
	
	# In case other external scripts want to set a cpuser
	if [[ -z $cpuser ]]; then
	cpuser=`whoami`
	fi

	if [[ $mode == 'manual' ]]; then
#MANUAL
		echo -e "\n[MANUAL]"
		echo -e "db user [pass] [host]"
		read -p "> " dbname_new dbuser_new dbpass_new dbhost_new

		# if no dbpass was entered, set it to a random string
		if [[ -z $dbpass_new ]]; then
			echo "No password given, assigning a random value..."
			dbpass_new=$random_pw_string
		fi

		# If the 4th argument isn't passed (dbhost_new), just set it to 'localhost'
		if [[ -z $dbhost_new ]]; then
			echo "No host given, setting as 'localhost'..."
			dbhost_new='localhost'
		fi

	elif [[ $mode == 'url' ]]; then
#URL
		echo "Maybe one day this will allow you to update the URL in a command. But it doesn't as of now. Bye!" 
		return

	else
#AUTO
		echo -e "\n[AUTO]"

		leftside=$cpuser"_"
		leftside_length=${#leftside}
		

	#DBNAME
		
		# if a default value is discovered, set it to something random
		if [[ $dbname_old == 'some_default_value' || $dbname_old == '' ]]; then
			dbname_old=$random_name_string
		fi
		dbname_rightside=${dbname_old#$leftside}
		if [[ $debug == 'true' ]]; then echo "dbname_rightside: $dbname_rightside"; fi
		dbname_new=$leftside$dbname_rightside

	#USERNAME	
		
		# if a default value is discovered, set it to something random
		if [[ $dbuser_old == 'some_default_value' || $dbuser_old == '' ]]; then
			dbuser_old=$random_name_string
		fi
		dbuser_rightside=${dbuser_old#$leftside}
		if [[ $debug == 'true' ]]; then echo "dbuser_rightside: $dbuser_rightside"; fi
		if (( ${#dbuser_rightside} > 16-$leftside_length )); then
			dbuser_new=$leftside${dbuser_rightside:$leftside_length-16}
		else
			dbuser_new=$leftside$dbuser_rightside
		fi

	#PASSWORD

		# if a default value is discovered, set it to something random
		if [[ $dbpass_old == 'some_default_value' || $dbpass_old == '' ]]; then 
			echo "dbpass_old was default value, changing to random string: '$random_pw_string'"
			dbpass_new=$random_pw_string
		
		# If it's too short, add some characters to the end
		elif (( ${#dbpass_old} < 6 )); then 
			echo "dbpass_old < 6 chars, adding random string: '$random_pw_string'"
			dbpass_new=$dbpass_old$random_pw_string #random_pw_string defined at the top, I use it in more than 1 place
		
		# otherwise, keep it
		else 
			echo "dbpass_old >= 6 chars, keeping old password"
			dbpass_new=$dbpass_old
		fi
	
	#HOST
		
		# host will always be 'localhost'
		dbhost_new='localhost'
	
	fi

	if [[ -z $dbuser_new ]]; then ## needs to be if no $dbuser_new or no $
		echo "No dbuser given, exiting with no changes"
	
	else
		# clean up right side for sed's use because backslashes, ampersands and pipes actually mean something to sed.
		for i in dbname_new dbuser_new dbpass_new dbhost_new; do
			declare safe_$i=$(printf '%s' "${!i}" | sed -e 's|\\|\\\\|g; s|&|\\\&|g; s/|/\\\|/g')
		done

		# Define paths
		pwd=`pwd`
		log_path_new=$pwd/logs #no trailing slash
		tmp_path_new=$pwd/tmp #no trailing slash

		# sed the crap out of everything
		sed -i -e "s|\$db\(\s*\)\?=\(\s*\)\?\(['\"]\).*\3|\$db\1=\2\3$safe_dbname_new\3|
		s|\$host\(\s*\)\?=\(\s*\)\?\(['\"]\).*\3|\$host\1=\2\3$safe_dbhost_new\3|
		s|\$user\(\s*\)\?=\(\s*\)\?\(['\"]\).*\3|\$user\1=\2\3$safe_dbuser_new\3|
		s|\$password\(\s*\)\?=\(\s*\)\?\(['\"]\).*\3|\$password\1=\2\3$safe_dbpass_new\3|
		s|\$log_path\(\s*\)\?=\(\s*\)\?\(['\"]\).*\3|\$log_path\1=\2\3$log_path_new\3|
		s|\$tmp_path\(\s*\)\?=\(\s*\)\?\(['\"]\).*\3|\$tmp_path\1=\2\3$tmp_path_new\3|" configuration.php

		# Set these values back to what the temp ones were. This shouldn't change anything except if the old values were default 'username_here' ones. 
		dbname_old=$dbname_old_temp
		dbuser_old=$dbuser_old_temp
		

		# spit out the info
		if [[ $dbhost_new == 'localhost' ]]; then dbhost_new=''; echo "omitting 'localhost' from output of new credentials..."; fi
		echo -e "\nOLD:\n   DB:       $dbname_old $dbuser_old $dbpass_old $dbhost_old\n   livesite: $live_site_old \n   log_path: $log_path_old\n   tmp_path: $tmp_path_old"
		echo -e "\nNEW:\n   DB:       $dbname_new $dbuser_new $dbpass_new $dbhost_new\n   livesite: $live_site_new \n   log_path: $log_path_new\n   tmp_path: $tmp_path_new"

	fi
	
}
